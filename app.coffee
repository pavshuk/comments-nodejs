config = require('./config')

webSocket = require('ws').Server
webSocketServer = new webSocket(config.get('daemon:webSocketServer'))
mongoDb = require('mongodb').MongoClient

log = (text)->
  console.log '['+(new Date()).toLocaleTimeString()+'] '+text


mongoDb.connect "mongodb://nodejitsu:b8e7aabbf2913b3a243c7b1e201c8067@dharma.mongohq.com:10047/nodejitsudb9584547914", (err, db) ->
#mongoDb.connect "mongodb://ds053658.mongolab.com:53658/heroku_app19592545", (err, db) ->
  throw err if err

  webSocketServer.broadcast = (data, key) ->
    for i of @clients
      clKey = @clients[i].objectForCommentKey
      if clKey.model == key.model and clKey.model_id == key.model_id
        @clients[i].send data


  controller = require('./Controller')(db, webSocketServer)

  console.log('Enter')

  webSocketServer.on('connection', (ws)->
    ws.on 'message', (data, type)->
      log 'Got : ' + data
      obj = JSON.parse(data)
      if obj.command and obj.params and  controller[obj.command]
        controller[obj.command](ws, obj.params)
      else
        log 'Got undefined command from client : ' + data

    ws.on 'close', ->
      log 'CDisconnect'

    log 'New connection'
  )

#http = require("http")
#port = 8080
#url = "http://localhost:" + port + "/"
#
## We can access nodejitsu enviroment variables from process.env
#
## Note: the SUBDOMAIN variable will always be defined for a nodejitsu app
#url = "http://" + process.env.SUBDOMAIN + ".jit.su/"  if process.env.SUBDOMAIN
#http.createServer((req, res) ->
#  res.writeHead 200,
#    "Content-Type": "text/plain"
#
#  res.write "hello, I know nodejitsu."
#  res.end()
#).listen port
#console.log "The http server has started at: " + url
