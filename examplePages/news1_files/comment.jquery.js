(function ($) {

    var commentSearch ='li.comment';
    var _option = {
        webSocketServer: "ws://localhost:8081/getComments",
        debug:false,
        key:{
            model:'',
            model_id:''
        },
        secureCode:''
    };

    var ws = null;

    var commentButton = function () {
        return ' <a class="addComment" href="#">Комментировать</a>';
    };


    var parseTree = function (data) {
        var data = data || [];
        var ul = $('<ul></ul>');
        urlReg = /((https?|ftp)\:\/\/)?([a-z0-9]{1})((\.[a-z0-9-])|([a-z0-9-]))*\.([a-z]{2,6})(\/?)/g;
        urlReg2 = /href="www/g
        //TODO не обрабатываю ссылки вида mail.ru
        // TODO работу с регулярными выражениями от сюда нужно убрать
        for (var i = 0; i < data.length; i++) {
            var li = $('<li class="comment"></li>');
            var com = data[i].comment;
            if (com) {
                li.append(' <span>' + com.text.replace(urlReg,'<a href="$&">$&</a>').replace(urlReg2, 'href="http://www') + '</span>')
                    .append('<br><i>Имя: ' + com.user.name + ' email: '+com.user.email+'</i>');
            }
            li.append(commentButton());
            li.data(data[i]);
            ul.append(li);
            if (data[i].subComments) {
                li.append(parseTree(data[i].subComments));
            }
        }
        return ul;
    }

    //Устанавливает полностью новый набор комментариев
    var setComments = function(data){

        $(mainDomElement).html(parseTree(data)).append(commentButton);

        $(mainDomElement).find('a.addComment').bind('click', function (event) {
            com = $(event.currentTarget).closest(commentSearch);

            if (com.length === 0) {
                com = mainDomElement;
            }

            form = com.find('#commentForm')
            if (form.length === 1) {
                if (com == form.closest(commentSearch)) return;
            }
            mainDomElement.find('#commentForm').remove();
            com.append('<div id="commentForm">Имя: <input type="text" name="user.name"/> email: <input type="text" name="user.email"/> <textarea name="text"></textarea><button>Сохранить</button><div>');
            com.find('#commentForm>button').bind('click', function (event) {

                controller.addComment($(this).closest('#commentForm'));
                return false;
            });
        });
    }

    var mainDomElement = null;
    //Инициализация комментариев
    $.fn.comments = function (option) {

        $.extend(_option, option);

        mainDomElement = this;

        createWebSocket();


        return this;
    }

    // некое подобие контроллера
    var controller = {
        allComments : function(params){
            setComments(params.subComments);
        },
        newComment : function(params){

            this.getAllComments();//TODO частичное Обновление
        },
        getAllComments: function(){
            var obj = {
                command: 'getAllComments',
                params: {
                    key:_option.key,
                    secureCode:''
                }
            };
            ws.sendObj( obj );
        },
        addComment : function (form) {
            //TODO make validation
            getPath = function (obj){
                var parentNode = obj.parents(commentSearch);
                var rez;
                if(parentNode.length>0 || obj.closest(commentSearch).length){
                    rez = [obj.parent().children(commentSearch).index(obj)];
                    rez = getPath(parentNode).concat(rez);
                }else{
                    rez = [];
                }
                return rez;
            };
            com = form.closest(commentSearch);

            var comment = {
                user: {
                    name: form.find('[name="user.name"]').val(),
                    email: form.find('[name="user.email"]').val()
                },
                text: form.find('[name="text"]').val()
            };
            var command={
                command:'addNewComment',
                params:{
                    comment:comment,
                    path: getPath(com)
                }
            }

            ws.sendObj(command);
            form.remove();
        }
    }



    var log = function(text){
        if(_option.debug){
            console.log('['+(new Date()).toLocaleTimeString()+'] '+ text);
        }
    }

    function createWebSocket() {
        ws = new WebSocket(_option.webSocketServer);

        ws.sendObj = function(obj){
            log('Send to server:');
            console.dir(obj);
            ws.send(JSON.stringify(obj));
        };

        ws.onopen = function () {
            log('Connected');
            controller.getAllComments(); // TODO возможны ситуации, когда ответ не приходит, нам нужно сделать проверку,
            // но это мало вероятно
        };

        ws.onclose = function (event) {
            setTimeout(function(){createWebSocket()}, 1000);
            console.dir(event);
        };

        ws.onmessage = function (event) {
            obj = JSON.parse(event.data);
            if(obj.params && obj.command && controller[obj.command] !== undefined){
                log('Получены данные : ');
                console.dir(obj);
                controller[obj.command](obj.params);
            }else{
                log('Undefined command from server: ');
                log(event.data);
            }
        };
    }
})(jQuery);