module.exports = (db, webSocketServer) ->

  commentsModule = require('./CommentsModule')(db)

  rez={}
  rez.addNewComment = (ws, params)->
    commentsModule.addNewComment(ws, params, (err, comment, path)->
      return console.log err if err
      console.log('Record saved')
      com =
        command:'newComment'
        params:{
          comment:comment
          path:path
        }
      webSocketServer.broadcast(JSON.stringify(com), ws.objectForCommentKey)
    )

  rez.getAllComments =(ws, params)->
    ws.objectForCommentKey = params.key
    commentsModule.getAllComments ws, (err, comments)->
      command =
        params:
          subComments:comments
        command:'allComments'
      ws.send JSON.stringify(command)

  return rez